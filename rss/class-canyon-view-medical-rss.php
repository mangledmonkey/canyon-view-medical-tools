<?php

/**
 * The Careers-specific functionality of the plugin.
 *
 * @link       http://mangledmonkeymedia.com
 * @since      1.2.0
 *
 * @package    Canyon_View_Medical
 * @subpackage Canyon_View_Medical/RSS
 */

/**
 * The Careers-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the Careers-specific stylesheet and JavaScript.
 *
 * @package    Canyon_View_Medical
 * @subpackage Canyon_View_Medical/RSS
 * @author     Mangled Monkey Media <brandon@mangledmonkeymedia.com>
 */
class Canyon_View_Medical_RSS {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the Careers area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Canyon_View_Medical_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Canyon_View_Medical_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name . '-rss', plugin_dir_url( __FILE__ ) . 'css/canyon-view-medical-rss.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name . '-blog', plugin_dir_url( __FILE__ ) . 'css/canyon-view-medical-blog-dropdown.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the RSS area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Canyon_View_Medical_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Canyon_View_Medical_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/canyon-view-medical-rss.js', array( 'jquery' ), $this->version, false );

	}

	
	/**
	 * Register the shortcodes for the public-facing side of the site.
	 *
	 * @since    1.2.0
	 */
	public function register_shortcodes() {

		add_shortcode( 'cvm_blog_feed', array( $this, 'cvm_blog_feed_shortcode' ) );
		add_shortcode( 'cvm_blog_dropdown', array( $this, 'cvm_blog_dropdown_shortcode' ) );

 	}

	/**
	 * Registers custom rss feed
	 */
	public function register_rss_feeds() {

		// add the 'specialties' custom feed for children blogs
		add_feed( 'specialties', array( $this, 'generate_rss_content' ) );

		// Refresh the rss feed in seconds
		add_filter( 'wp_feed_cache_transient_lifetime', function(){ return 5; });

	}

	/**
	 * Registers custom Rest API endpoints
	 * 
	 * @since		1.3.0
	 */
	public function register_api_routes() {

		// add the 'list-archives' rest api endpoint
		register_rest_route( 'cvm/v1', 'list-archives', array(
			'methods'  => 'GET',
			'callback' => array( $this, 'list_archives' ))
		);
	}


	/**
	 * Generates the content of the custom rss feed
 	 *
	 * @since    1.2.00.
	 */
	function generate_rss_content() {

		if ( file_exists( ABSPATH . WPINC . '/feed.php' ) ) {
		 	include_once( ABSPATH . WPINC . '/feed.php' );
		}
		
		require( plugin_dir_path( __FILE__ ) . 'partials/canyon-view-medical-rss-feed.php' );
	
	}

		/**
	 * Returns only the primary specialty category
 	 *
	 * @since    1.2.04.
	 */
	function get_primary_specialty( $categories ) {
		$primary = '';
		$secondary = array();

		foreach ( $categories as $category ) {
			// $ret .= $post_category->name;
			if (substr( $category->slug, 0, 12 ) === 'canyon-view-') {
				if( get_post_meta(get_the_ID(), '_yoast_wpseo_primary_category',true) == $category->term_id ) {
					$primary = $category;
				}
			}	else {
				array_push($secondary, $category);
			}
		}

		return array($primary, $secondary);
	}

	/**
	 * Returns local or remote domain URL depending on environment.
	 *
	 * @since    1.3.00
	 */
	function get_env_uri()
	{
		if ( strpos( get_site_url(), 'localhost' ) !== false ) :
			$domain = 'http://localhost:80';
		else:
			$domain = 'https://canyonviewmedical.com';
		endif;

		return $domain;
	}

	/**
	 * Returns archives dropdown list by month.
	 *
	 * @since    1.3.00
	 */
	function list_archives()
	{
		$args = array(
			'type'            => 'monthly',
			'limit'           => '',
			'format'          => 'option', 
			'before'          => '',
			'after'           => '',
			'show_post_count' => false,
			'echo'            => 1,
			'order'           => 'DESC',
			'post_type'     	=> 'post'
		);
		return wp_get_archives( $args );
	}

	/**
	 * Returns booleon check on Medical domain.
	 *
	 * @since    1.4.03
	 */
	function is_medical()
	{
		$value = false;
		$url = get_site_url();
		if ( strpos( $url, 'localhost' ) !== false ) :
			$value = true;
		elseif (strpos( $url, 'canyonviewmedical.com' ) !== false ):
			$value = true;
		endif;

		return $value;
	}

	/**   SHORTCODES   **/

	/**
	 * shortcode [cvm_blog_feed category]
	 * 
	 * Presents the most recent $count posts from the
	 * Canyon View Medical blog RSS feed that carry the
	 * primary $category specified.
	 *
	 * @since    1.2.00
	 */
	public function cvm_blog_feed_shortcode( $specialty )
	{
		$specialty_slug = $specialty[0];
		$numposts = 5;

		$domain = $this->get_env_uri();

		$feedUrl = $domain . '/news-blog/feed/specialties/?specialty=' . $specialty_slug . '&numposts=' . $numposts;

		// //Get RSS Feed(s)
		// if ( file_exists( ABSPATH . WPINC . '/feed.php' ) ) {
		// 	include_once( ABSPATH . WPINC . '/feed.php' );
		// }

		// Get a feed object from Canyon View Medical.
		$rss = fetch_feed( $feedUrl );

		if ( ! is_wp_error( $rss ) ) : // Checks that the object is created correctly
 
			// Figure out how many total items there are, but limit it to 5. 
			$maxitems = $rss->get_item_quantity( $numposts ); 
	 
			// Build an array of all the items, starting with element 0 (first element).
			$rss_items = $rss->get_items( 0, $maxitems );

		else : // print error
			echo 'Feed URL: ' . $feedUrl;
			echo 'RSS error: ' . $rss->get_error_message();
	 
		endif;

		require( plugin_dir_path( __FILE__ ) . 'partials/canyon-view-medical-rss-display.php' );
	

	}
	

	/**
	 * shortcode [cvm_blog_dropdown item]
	 * 
	 * Returns and presents a dropdown menu of categories, specialties,
	 * or previous months as defined by $item.
	 *
	 * @since    1.3.00
	 */
	public function cvm_blog_dropdown_shortcode( $item )
	{
		global $wp_query;
		// $category = get_category( get_query_var( 'cat' ) );
		// error_log('category: ' . $wp_query['cat'] );
		$domain = $this->get_env_uri();
		$departments = array();
		$port = ( strpos( $domain, 'localhost' ) !== false ? '80' : '' );
		
		if ( $this->is_medical() === true ) {
			$on_change = "window.location.href=this.options[this.selectedIndex].value";
		} else {
			$on_change = "window.open(this.options[this.selectedIndex].value)";
		}

		switch( $item[0] ){
			case 'departments':
				$remote = '';
				$label = 'Department';
				$ref = 'dep';
				$category = get_category( $wp_query->query['meta_query'][0]['value'] );
				$departments = [
					['name' => 'Canyon View Medical', 'slug' => 'canyon-view-medical'],
					['name' => 'Canyon View Family Medicine', 'slug' => 'canyon-view-family-medicine'],
					['name' => 'Canyon View Pediatrics', 'slug' =>  'canyon-view-pediatrics'],
					['name' => 'Canyon View Women\'s Care', 'slug' =>  'canyon-view-womens-care'],
					['name' => 'Canyon View Orthopedics', 'slug' =>  'canyon-view-orthopedics'],
					['name' => 'Canyon View Sports Medicine', 'slug' =>  'canyon-view-sports-medicine']
				];

				break;
			case 'categories':
				$remote = $domain . '/wp-json/wp/v2/categories?per_page=100';
				$label = 'Category';
				$ref = 'cat';
				$category = get_category( get_query_var( 'cat' ) );
				break;
			case 'archives':
				$remote = $domain . '/wp-json/cvm/v1/list-archives';
				$label = 'Month';
				$ref = 'arc';
				$year = $wp_query->query['year'];
				$month = $wp_query->query['monthnum'];
				$category = date('F Y', strtotime($year . '-' . $month . '-04')	);
				break;
		}

		if ( $remote !== '' ) {
			$request = wp_remote_get( $remote );

			if( is_wp_error( $request ) ) {
				return false; // Bail early
			}
			
			$body = wp_remote_retrieve_body( $request );

			switch( $ref ) {
				case 'cat':
					$categories = json_decode( $body );
					break;
				case 'arc':
					$archives = explode("<option", $body);
					$categories = array();
					foreach( $archives as $archive ) {
						$matches = null;
						if(preg_match('/value=\'(.*)\'.*?>\ (.*)\ <\/option>/', $archive, $matches)) {
							$match = array(
								'url' => $matches[1],
								'name' => $matches[2]
							);
							array_push( $categories, $match );
						}
					}
					break;
			}
			
			if( ! empty( $categories ) ) {
	
				require( plugin_dir_path( __FILE__ ) . 'partials/canyon-view-medical-blog-dropdown-' . $ref . '.php' );
	
			}
		} else {

			require( plugin_dir_path( __FILE__ ) . 'partials/canyon-view-medical-blog-dropdown-' . $ref . '.php' );	

		}

	}
	
}
