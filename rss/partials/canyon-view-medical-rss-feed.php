<?php
/**
 * Provide a custom rss feed format
 * 
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://mangledmonkeymedia.com
 * @since      1.2.0
 *
 * @package    Canyon_View_Medical
 * @subpackage Canyon_View_Medical/RSS/partials
 */
?>
<?php
  global $wp_query;
  $domain = "https://canyonviewmedical.com";
  $more = 1;
  $word_limit = 50;
  $specialty = sanitize_text_field( $_GET['specialty'] );
  $category = get_category_by_slug( $specialty );
  $numposts = sanitize_text_field( $_GET['numposts'] ); // number of posts in feed
  $primary_only = sanitize_text_field( $_GET['primary_only'] ); // filter out non-primary specialties
  $posts = array();
  $query_args = array();
  
  // filter out non-primary specialties
  if ( $primary_only === '1'  ) {
    $query_args = array(
      'posts_per_page' => $numposts,
      'post_status' => 'publish',
      'meta_query'  => array(
        'relation' => 'AND',
        array(
          'key'     => '_yoast_wpseo_primary_category',  
          'value'   => $category->term_id,
          'compare' => '=',
        ),
        array(
          'key'     => '_yoast_wpseo_primary_category',  
          'value'   => '',
          'compare' => '!=',
        ),
      ),
    );
  } else {
    $query_args = '&showposts='.$numposts.'&cat='.$category->term_id;
  }
  $posts = query_posts( $query_args );

  header('Content-Type: '.feed_content_type('rss-http').'; charset='.get_option('blog_charset'), true);
  echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>';
?>
		
  <rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:wfw="http://wellformedweb.org/CommentAPI/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
    <?php do_action('rss2_ns'); ?>
  >
  <channel>
    <title><?php bloginfo_rss('name'); wp_title_rss(); ?> - Specialties Feed</title>
    <atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
    <link><?php bloginfo_rss('url') ?></link>
    <description><?php bloginfo_rss("description") ?></description>
    <lastBuildDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_lastpostmodified('GMT'), false); ?></lastBuildDate>
    <?php the_generator( 'rss2' ); ?>
    <language><?php echo get_option('rss_language'); ?></language>
    <sy:updatePeriod><?php echo apply_filters( 'rss_update_period', 'hourly' ); ?></sy:updatePeriod>
    <sy:updateFrequency><?php echo apply_filters( 'rss_update_frequency', '1' ); ?></sy:updateFrequency>
    <?php do_action('rss2_head'); ?>

    <?php while( have_posts()) : the_post(); ?>
      <?php $content = trim( strip_tags( get_the_content() ) ); ?>
      <?php $content = str_replace( '&nbsp;', '', $content ); ?>
      <?php $content_words = explode(" ", $content); ?>
      <?php $content_words = implode(" ", array_splice($content_words,0,$word_limit)); ?>
      <?php $content_excerpt = preg_replace( '/https?:\/\/(?:www\.)?vimeo\.com\/\d{9}/', '', $content_words ); ?>
      <?php $content_excerpt = preg_replace( '/(?:https?:\/\/)?(?:www\.)?youtu(?:\.be|be\.com)\/(?:watch\?v=)?([\w-]{10,})/m', '', $content_excerpt ); ?>
      <?php list($primary, $categories) = $this->get_primary_specialty( get_the_category() ); ?>
    

      <item>
        <title><?php the_title_rss(); ?></title>
        <link><?php the_permalink_rss(); ?></link>
        <comments><?php comments_link(); ?></comments>
        <pubDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_post_time('Y-m-d H:i:s', true), false); ?></pubDate>
        <dc:creator><?php the_author(); ?></dc:creator>

      <?php 
        if ( $primary !== '') {
          echo '<category><![CDATA[' . $primary->name . ']]></category>';
        }
        foreach ( $categories as $category ) {
          echo '<category><![CDATA[' . $category->name . ']]></category>';
        }
      ?>

        <guid isPermaLink="false"><?php the_guid(); ?></guid>
    
        <description><![CDATA[<?php echo trim($content_excerpt); ?>]]></description>
    
        <wfw:commentRss><?php echo get_post_comments_feed_link(); ?></wfw:commentRss>
        <slash:comments><?php echo get_comments_number(); ?></slash:comments>

        <?php // if ( get_post_format( get_the_ID() ) === 'video' ) : ?>
        <?php // if ( preg_match('/https?:\/\/(?:www\.)?vimeo\.com\/\d{9}/', $content) ) {
          // preg_match('/https?:\/\/(?:www\.)?vimeo\.com\/\d{9}/', $content, $match);
        // } else {
          // preg_match( '/(?:https?:\/\/)?(?:www\.)?youtu(?:\.be|be\.com)\/(?:watch\?v=)?([\w-]{10,})/m', $content, $match ); 
        // } ?>

        <!-- <enclosure url="<?php echo $match[0] ?>" length="100000" type="video/mpeg"/> -->
        <!-- <media:content xmlns:media="http://search.yahoo.com/mrss/" url="<?php echo $match[0] ?>" medium="video" isDefault="true"> -->
        <!-- </media:content> -->

      <?php // elseif ( has_post_thumbnail( $post->ID ) ) :
        $image_size = 'medium';
        $upload_dir = wp_upload_dir();
        $image_id =  get_post_thumbnail_id();
        $image_meta = wp_get_attachment_metadata($image_id);
        $image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
        $image_file = $image_meta[ 'file' ];
        $dirname = dirname( $image_file );
        $base_url = trailingslashit ( $upload_dir['baseurl'] ) . $dirname . '/';
        $thumbnail_info = $image_meta['sizes'][$image_size];
        $thumbnail_filename = basename( $thumbnail_info[ 'file' ] );
        $width = $thumbnail_info['width'];
        $height = $thumbnail_info['height'];
        $image_url = get_the_post_thumbnail_url( get_the_ID() );
        $thumbnail_url = $base_url . $thumbnail_filename;
        
      ?>

        <enclosure url="<?php echo $image_url ?>" length="" type="image/jpg"/>
        <media:content xmlns:media="http://search.yahoo.com/mrss/" url="<?php echo $image_url ?>" width="<?php echo $width ?>" height="<?php echo $height ?>" medium="image" type="image/jpeg">
          <media:copyright><?php echo $image_meta['image_meta']['copyright']; ?></media:copyright>
          <media:title><?php echo $image_meta['image_meta']['title']; ?></media:title>
          <media:caption type="html"><![CDATA[<?php echo $image_meta['image_meta']['caption']; ?>]]></media:caption>
          <media:description></media:description>
          <media:thumbnail url="<?php echo $thumbnail_url ?>" medium="image"></media:thumbnail>
        </media:content>
      <?php // endif; ?>
<!-- 
      <?php rss_enclosure(); ?>
      <?php do_action('rss2_item'); ?> -->
  
    </item>
    <?php endwhile; ?>
  
  </channel>
  </rss>
