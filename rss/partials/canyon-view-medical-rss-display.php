<?php
/**
 * Provide a custom rss feed format
 * 
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://mangledmonkeymedia.com
 * @since      1.0.0
 *
 * @package    Canyon_View_Medical
 * @subpackage Canyon_View_Medical/RSS/partials
 */
?>
<?php // Create goodlayer post feed content layout ?>
<div class="blog-item-wrapper">
  <div class="blog-item-holder">
    <div class="gdlr-isotope" data-type="blog" data-layout="fitRows">


<?php if ( $maxitems == 0 ) : ?>
  <li><?php _e( "No items", "my-text-domain" ) ?></li>
<?php else : // Loop through each feed item and display each item as a hyperlink. ?>
  <?php foreach ( $rss_items as $item ) : 
    $author = $item->get_author();
    $article_url = $item->get_permalink();
    // set up item class tags
    // $itemTags = $item->get_item_tags();
    // $classTags = '';
    // foreach ( $itemTags as $tag ) :
    // 	$classTags .= ' tag-' . $tag;
    // endforeach
    $specialties = array();
    $post_categories = $item->get_categories();

    foreach ( $post_categories as $post_category ) {
      // $ret .= $post_category->name;
      if (substr( $post_category->get_label(), 0, 12 ) === 'Canyon View ') {
        array_push( $specialties, $post_category );
      }
    }
  ?>
    <!-- // goodlayer grid layout -->
    <div class="clear"></div>
      <div class="twelve columns">
        <div class="gdlr-item gdlr-blog-grid">
          <div class="gdlr-ux gdlrr-blog-grid-ux">

            <!-- // post item -->
            <article id="post-<?php echo $item->get_id() ?>" class="post type-post format-standard" >
              <div class="gdlr-standard-style">

                <!-- // post thumbnail -->
              <?php if ( $enclosure = $item->get_enclosure() ) : ?>
                <?php $medium = $enclosure->get_medium(); ?>

                <?php if ( $medium === 'image' ) : ?>
                <div class="gdlr-blog-thumbnail">
                  <a href="<?php echo $article_url ?>" target="_blank">
                    <img src="<?php echo $enclosure->get_link() ?>">
                  </a>
                </div>
                <?php elseif ( $medium === 'video' ) : ?>
                <div class="gdlr-blog-thumbnail gdlr-video">
                  <div class="gdlr-fluid-video-wrapper">
                    <?php echo do_shortcode('[arve url="' . $enclosure->get_link() . '"]'); ?>
                  </div>
                </div> 
                <?php endif ?>
              <?php endif ?>

                <!-- // post info -->
                <div class="gdlr-blog-info gdlr-info">
                  <div class="blog-info blog-date">
                    <i class="fa fa-clock-o"></i>
                    <a href="" target="_blank"><?php echo $item->get_date() ?></a>
                  </div> <?php // blog-date ?>
                  <div class="blog-info blog-author">
                    <i class="fa fa-pencil"></i>
                    <a href="" target="_blank"><?php echo $author->get_name() ?></a>
                  </div> <?php // blog-author ?>

                <?php foreach ( $specialties as $specialty ) { ?>
                  <?php $specialty = $specialty->get_label(); ?>
                  <?php $slug = str_replace(' ', '-', strtolower($specialty)); ?>
                  <?php $slug = str_replace("'", '', $slug ); ?>
                  <div class="blog-info blog-specialty">
                    <a href="https://canyonviewmedical.com/news-blog/categories/<?php echo $slug ?>/" target="_blank">
                      <i class="specialty specialty-<?php echo $slug ?>"></i>
                    </a>
                  </div>
                <?php } ?>

                  <div class="clear"></div>
                </div> <?php // gdlr-blog-info ?>

                <!-- // post header -->
                <header class="post-header">
                  <h3 class="gdlr-blog-title gdlr-content-font">
                    <a href="<?php echo $article_url ?>" target="_blank"><?php echo $item->get_title() ?></a>
                  </h3>
                  <div class="clear"></div>
                </header>
    
                <!-- // post content -->
                <div class="gdlr-blog-content">
                  <?php echo $item->get_description() . '&#8230;' ?>
                  <div class="clear"></div>
                  <a class="excerpt-read-more" href="<?php echo $article_url ?>" target="_blank">Read More</a>
                </div><?php // gdlr-blog-content ?>

              </div><?php // gdlr-standard-style ?>
            </article>

    <!-- // close goodlayer grid layout -->
          </div><?php // gdlr-ux ?>
        </div><?php // gdlr-item ?>
      </div><?php // column class ?>
  <?php endforeach ?>
<?php endif ?>


      <div style="text-align:center;padding:20px;">
        <a href="https://canyonviewmedical.com/news-blog/" style="color:#8A2575;" target="_blank">Read more articles at Canyon View Medical</a>
      </div>

<!-- // close goodlayer post feed content layout -->
    </div><?php // close the gdlr-isotope ?>
  </div><?php // blog-item-holder ?>
</div><?php // blog-item-wrapper ?>