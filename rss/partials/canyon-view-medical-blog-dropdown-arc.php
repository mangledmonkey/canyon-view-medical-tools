<?php
/**
 * Provide a dropdown menu for the blog sidebar
 * 
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://mangledmonkeymedia.com
 * @since      1.3.0
 *
 * @package    Canyon_View_Medical
 * @subpackage Canyon_View_Medical/RSS/partials
 */
?>
<div id="article-<?php echo $ref ?>" class="widget widget_archive gdlr-item gdlr-widget">
  <div class="clear"></div>
  <label class="screen-reader-text" for="<?php echo $ref ?>"><?php echo ucfirst( $item[0] ); ?></label><br />
  <select name="<?php echo $ref ?>" id="<?php echo $ref ?>" class="blog-dropdown postform" onChange="<?php echo $on_change ?>"> 
    <option value="-1">Select <?php echo attribute_escape(__($label)) ?></option>
		<option value="<?php echo $domain; echo $port ?>/news-blog/">All Archives</option>
    <?php foreach ( $categories as $cat ) { ?>
      <?php if( $cat['name'] === $category ) {
          $selected = 'selected';
        } else {
          $selected = '';
        }
      ?>
		<option class="level-0" value="<?php echo esc_url( $cat['url'] ) ?>" <?php echo $selected ?>><?php echo $cat['name'] ?></option>
		<?php } ?>
  </select>
</div>