jQuery(document).ready(function($){

  var mediaUploader;

  $('#cvfmp_meta_provider_image_upload_button').click(function(e) {
    e.preventDefault();
    openMediaUploader('#cvfmp-meta-provider-image-img', '#cvfmp-meta-provider-image-url');
   });


  function openMediaUploader(img, input){
    // If the uploader object has already been created, reopen the dialog
    // if (mediaUploader) {
    //   mediaUploader.open();
    //   return;
    // }
    // Extend the wp.media object
    mediaUploader = wp.media.frames.file_frame = wp.media({
      title: 'Choose Image',
      button: {
        text: 'Choose Image'
      },
      multiple: false
    });

    var set_media_image = function() {
      var selection = mediaUploader.state().get('selection');

      // no selection
      if (!selection) {
        return;
      }

      // iterate through selected elements
      selection.each(function(attachment) {
        var url = attachment.attributes.url;
        $(input).val(url);
        $(img).attr('src', url);
      });
    };


    mediaUploader.on('select', set_media_image);
    mediaUploader.on('close', set_media_image);

    // Open the uploader dialog
    mediaUploader.open();
  }
});
