<?php

/**
 * Provide a admin area view for procedure pricing feature of the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://mangledmonkeymedia.com
 * @since      1.0.0
 *
 * @package    Canyon_View_Medical
 * @subpackage Canyon_View_Medical/procedure-pricing/partials
 */
?>
<div class="wrap">
  <div id="cvm_procedure_pricing_display">
    <h2>Canyon View Medical—Procedure Pricing</h2>
    <p>To display the final list, place the shortcode <code>[cvm_procedure_pricing]</code> in a page or post content field.</p>

    <section id="buildList">
      <h3>Build the Procedure Pricing list</h3>
      <form method="post" action="options.php">
      <?php settings_fields( 'cvm-procedure-pricing' ); ?>
      <?php do_settings_sections( 'cvm-procedure-pricing' ); ?>

        <div id="cvm-procedure-pricing-sections">

          <ul>
            </lh>
            <li>
              <label for="cvm-procedure-pricing-sections" class="cvm-row-title">Section: </label>
              <input type="text" name="_cvm_sections" value="<?php echo esc_attr( get_option('new_option_name') ); ?>"/>
            </li>
          </ul>
        <?php
          // $sections = get_option( '_cvm_sections', $default = false );
          // if ( $sections != '' ) {
            // foreach ( $sections as $section ) {
              // printf( $section . '<br>');
            // }
          // }

        ?>

        </div>

        <button id="cvm-precedure-pricing-new-section-button" class="button cvm-new-section" type="button">+ New Section</button>

        <?php submit_button(); ?>
        
      </form>


    </section>

  </div>
</div>
