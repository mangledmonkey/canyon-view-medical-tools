<?php

/**
 * The procedure-pricing-specific functionality of the plugin.
 *
 * @link       http://mangledmonkeymedia.com
 * @since      1.0.0
 *
 * @package    Canyon_View_Medical_Procedure_Pricing
 * @subpackage Canyon_View_Medical_Procedure_Pricing/procedure-pricing
 */

/**
 * The procedure-pricing-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the procedure-pricing-specific stylesheet and JavaScript.
 *
 * @package    Canyon_View_Medical_Procedure_Pricing
 * @subpackage Canyon_View_Medical_Procedure_Pricing/procedure-pricing
 * @author     Brandon West <brandon@mangledmonkeymedia.com>
 */
class Canyon_View_Medical_Procedure_Pricing {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the procedure-pricing area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Canyon_View_Medical_Procedure_Pricing_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Canyon_View_Medical_Procedure_Pricing_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/canyon-view-medical-procedure-pricing.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the procedure-pricing area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Canyon_View_Medical_Procedure_Pricing_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Canyon_View_Medical_Procedure_Pricing_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name,  plugin_dir_url( __FILE__ ) . 'js/canyon-view-medical-procedure-pricing.js', array( 'jquery' ), $this->version, false );
		// wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/media-file-uploader.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Add the procedures pricing admin page
	 *
	 * @since		1.1.0
	 */
	public function admin_menu() {
		add_menu_page( 'Canyon View Medical—Procedure Pricing', 'procedure-pricing', 'manage_options', 'canyon-view-medical/procedure-pricing/partials/canyon-view-medical-procedure-pricing-display.php', '', '', 21);
	}

	/**
	 * Register the settings for procedures pricing admin page
	 *
	 * @since		1.1.0
	 */
	public function register_settings() {
		register_setting( 'cvm-procedure-pricing', '_cvm_sections' );
	}


	/**
	 * Register the shortcodes for the public-facing side of the site.
	 *
	 * @since    1.2.0
	 */
	public function register_shortcodes() {

		add_shortcode( 'cvm_procedure_pricing', array( $this, 'cvm_procedure_pricing_shortcode' ) );

 	}


	/**
	 * Add the procedures meta box to the page
	 *
	 * @since		1.0.0
	 */
	public function add_cvm_procedure_pricing_meta_box() {
		global $post;
		$pageName = get_the_title($post->ID);

		if ( $pageName == "Clinic Pricing") {
			add_meta_box(
				'cvm_procedure_pricing_meta_box',
				__( 'Canyon View Medical Procedure Pricing', 'cvm_procedure_pricing_textdomain' ),
				array( $this, 'cvm_procedure_pricing_meta_box_callback' ),
				'page'
			);
		}

	}

	/**
	 * Add a procedure to a section list
	 *
	 * @since		1.2.0
	 */
	function print_cvm_procedure_listing( $procedure, $cnt, $p ) {
		?>
				<tr id="cvm-procedure-<?php echo $p; ?>">
					<td>
						<input type="text" name="cvm_procedure_pricing_meta_sections[<?php echo $cnt; ?>][procedures][<? echo $p; ?>][cpt-code]" value="<?php echo esc_attr( $procedure['cpt-code'] ); ?>" size="5" />
					</td>
					<td>
						<input type="text" name="cvm_procedure_pricing_meta_sections[<?php echo $cnt; ?>][procedures][<? echo $p; ?>][name]" value="<?php echo esc_attr( $procedure['name'] ); ?>" size="40" />
					</td>
					<td>
						<input type="text" name="cvm_procedure_pricing_meta_sections[<?php echo $cnt; ?>][procedures][<?php echo $p; ?>][price]" value="<?php echo esc_attr( $procedure['price'] ); ?>" size="5" />
					</td>
					<td>
						<button class="button cvm cvm-remove-procedure" type="button">remove</button>
					</td>
				</tr>
		<?php
	}

	/**
	 * Add a section to the meta box
	 *
	 * @since		1.2.0
	 */
	function print_cvm_procedures_section($section, $cnt) {
		$procedures = $section['procedures'];
		?>
			<div id="cvm-section-<?php echo $cnt; ?>" class="cvm-section">
				<div class="cvm-section-header">
					<input type="text" id="hidden-section-heading-<?php echo $cnt; ?>" 
								name="cvm_procedure_pricing_meta_sections[<?php echo $cnt; ?>][heading]" 
								value="<?php echo esc_attr( $section['heading'] ); ?>" 
								class="cvm-section-heading" 
								size="50" />
				</div>
				<table class="rootlist">
					<thead>
						<th width="10%">CPT Code</th>
						<th width="70%">Procedure</th>
						<th width="10%">Price ($)</th>
						<th width="10%"></th>
					</thead>
					<tbody>
					<?php if (!empty($procedures))
						{
							usort($procedures, function($a, $b){ return strcmp($a['name'], $b['name']); });
							$count = count($procedures);
							
							for ($p = 0; $p < $count; $p++ ) {
								$procedure = $procedures[$p];
								$this->print_cvm_procedure_listing($procedure, $cnt, $p);
							}
						}
					?>
					</tbody>
					</table>
				<button class="button cvm cvm-new-procedure" type="button">+ New Procedure</button>
				<button class="button cvm cvm-remove-section" type="button">Remove Section</button>
			</div>
		<?php
	}

	/**
	 * Prints the meta box content.
	 *
	 * @param WP_Post $post The object for the current post/page.
	 */
	public function cvm_procedure_pricing_meta_box_callback( $post ) {

		// Add a nonce field so we can check for it later.
		wp_nonce_field( 'cvm_procedure_pricing_meta_box', 'cvm_procedure_pricing_meta_box_nonce' );

		/*
		 * Use get_post_meta() to retrieve an existing value
		 * from the database and use the value for the form.
		 */
		$sections = get_post_meta( $post->ID, '_cvm_procedure_pricing_meta_sections', true );
		is_array( $sections ) ? : $sections = [];
		$useShortcode = get_post_meta( $post->ID, '_cvm_procedure_pricing_meta_use_shortcode', true );

  ?>
  	<div id="cvm_procedure_pricing_meta_box">

			<p class="use-shortcode">
				<input type="checkbox" name="cvm_procedure_pricing_meta_use_shortcode" value="yes" <?php if ( isset ( $useShortcode ) ) checked( $useShortcode, 'yes' ); ?> />
				<label for="cvm-procedure-pricing-use-shortcode" class="cvm-row-title">Use shortcode for placement <code>[cvm_procedure_pricing]</code></label>
			</p>

		<? if ( empty( $sections ) ) { ?>
			<?php
			$emptySection = array(
												'heading' => 'Section Title',
												'procedures' => array(
													0 => array (
														'name' => '',
														'price' => ''
													)
												)
											);
			$this->print_cvm_procedures_section($emptySection, 0)
			?>

		<?php
					} else {
		?>
			<div id="cvm_procedure_sections">
		<?php
				for ($i = 0; $i < count($sections); $i++) {
					$this->print_cvm_procedures_section($sections[$i], $i);
			 	}
	   }
		 ?>
	 		</div>
      <button id="cvm-precedure-pricing-new-section-button" class="button cvm cvm-new-section" type="button">+ New Section</button>

  	</div>
  <?php
	}


	/**
	 * Save the meta when the post is saved.
	 *
	 * @param int $post_id The ID of the post being saved.
	 */
	public function save_cvm_procedure_pricing_meta_box( $post_id ) {

		/*
		 * We need to verify this came from the our screen and with proper authorization,
		 * because save_post can be triggered at other times.
		 */

		// Check if our nonce is set.
		if ( ! isset( $_POST['cvm_procedure_pricing_meta_box_nonce'] ) )
			return $post_id;

		$nonce = $_POST['cvm_procedure_pricing_meta_box_nonce'];

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $nonce, 'cvm_procedure_pricing_meta_box' ) )
			return $post_id;

		// If this is an autosave, our form has not been submitted,
	            //     so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return $post_id;

		// Check the user's permissions.
		if ( 'page' == $_POST['post_type'] ) {

			if ( ! current_user_can( 'edit_page', $post_id ) )
				return $post_id;

		} else {

			if ( ! current_user_can( 'edit_post', $post_id ) )
				return $post_id;
		}

		/* OK, its safe for us to save the data now. */

	  // Sanitize the user input.
	  $sections = $_POST['cvm_procedure_pricing_meta_sections'];
		$useShortcode = $_POST['cvm_procedure_pricing_meta_use_shortcode'];

	  // Update the meta field.
		update_post_meta( $post_id, '_cvm_procedure_pricing_meta_sections', $sections );
		update_post_meta( $post_id, '_cvm_procedure_pricing_meta_use_shortcode', $useShortcode );

	}



	/**
	 * shortcode [cvm_procedure_pricing]
	 *
	 * @since    1.0.0
	 */
	public function cvm_procedure_pricing_shortcode ()
  {
		global $post;

    if ( $post->post_title == "Clinic Pricing" )
    {
      $sections = get_post_meta( $post->ID, '_cvm_procedure_pricing_meta_sections', 1);
			$lastUpdated = get_the_date();

			if ( !empty($sections) )
			{
				$procedureContent = '<div class="gdlr-item gdlr-accordion-item style-2">';
				$count = 0;

				foreach ( $sections as $section )
				{
					$heading = $section['heading'];
					$procedures = $section['procedures'];
				
					usort($procedures, function($a, $b){ return strcmp($a['name'], $b['name']); });

					if ( $count == 0 )
					{
						$isActive = ' active pre-active';
						$count = 1;
					} else {
						$isActive = '';
					}

					$procedureContent .= '
							<div class="accordion-tab' . $isActive . '">
								<h4 class="accordion-title"><i class="icon-minus"></i><span>' . $heading .'</span></h4>
								<div class="accordion-content">
									<ul class="pricing">';

					foreach ( $procedures as $procedure )
					{
						$procedureContent .= '
										<li><span class="cpt-code">' . $procedure['cpt-code'] . '</span> <span class="procedure-name">' . $procedure['name'] . '</span> <span class="price">$'. $procedure['price'] . '</span></li>';
					}

					$procedureContent .= '
									</ul>
								</div>
							</div>';
				}
				$procedureContent .= '
							<span class="last-updated">Last updated ' . $lastUpdated . '</span>';

				$procedureContent .= '
						</div>';
			}
    }

    return $procedureContent;
  }


	/**
	 * Prints the procedure pricings info to live page.
	 *
	 * @param WP_Post $post The object for the current post/page.
	 */
	public function display_cvm_procedure_pricing ( $content )
  {
		global $post;

    if ( $post->post_title == "Clinic Pricing" )
    {
      $useShortcode = get_post_meta( $post->ID, '_cvm_procedure_pricing_meta_use_shortcode', 1);

      if ( $useShortcode != 'yes' )
      {
        $content .= '[cvm_procedure_pricing]';
      }
    }
		
    return $content;
  }

}
