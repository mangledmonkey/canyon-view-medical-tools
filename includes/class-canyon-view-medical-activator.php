<?php

/**
 * Fired during plugin activation
 *
 * @link       http://mangledmonkeymedia.com
 * @since      1.0.0
 *
 * @package    Canyon_View_Medical
 * @subpackage Canyon_View_Medical/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Canyon_View_Medical
 * @subpackage Canyon_View_Medical/includes
 * @author     Mangled Monkey Media <brandon@mangledmonkeymedia.com>
 */
class Canyon_View_Medical_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
