<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://mangledmonkeymedia.com
 * @since      1.0.0
 *
 * @package    Canyon_View_Medical
 * @subpackage Canyon_View_Medical/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Canyon_View_Medical
 * @subpackage Canyon_View_Medical/includes
 * @author     Mangled Monkey Media <brandon@mangledmonkeymedia.com>
 */
class Canyon_View_Medical {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Canyon_View_Medical_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'canyon-view-medical';
		$this->version = '1.7.9';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Canyon_View_Medical_Loader. Orchestrates the hooks of the plugin.
	 * - Canyon_View_Medical_i18n. Defines internationalization functionality.
	 * - Canyon_View_Medical_Admin. Defines all hooks for the admin area.
	 * - Canyon_View_Medical_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-canyon-view-medical-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-canyon-view-medical-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-canyon-view-medical-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the career area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'careers/class-canyon-view-medical-careers.php';

		/**
		 * The class responsible for defining all actions that occur in the procedure-pricing tool.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'procedure-pricing/class-canyon-view-medical-procedure-pricing.php';

		/**
		 * The class responsible for defining all actions that occur in the  tool.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'rss/class-canyon-view-medical-rss.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-canyon-view-medical-public.php';

		$this->loader = new Canyon_View_Medical_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Canyon_View_Medical_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Canyon_View_Medical_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Canyon_View_Medical_Admin( $this->get_plugin_name(), $this->get_version() );
		$plugin_careers = new Canyon_View_Medical_Careers( $this->get_plugin_name(), $this->get_version() );
		$plugin_procedure_pricing = new Canyon_View_Medical_Procedure_Pricing( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		// $this->loader->add_action( 'admin_enqueue_scripts', $plugin_careers, 'enqueue_styles' );
		// $this->loader->add_action( 'admin_enqueue_scripts', $plugin_careers, 'enqueue_scripts' );
		// $this->loader->add_action( 'admin_enqueue_scripts', $plugin_procedure_pricing, 'enqueue_styles' );
		// $this->loader->add_action( 'admin_enqueue_scripts', $plugin_procedure_pricing, 'cvm_procedure_pricing_enqueue_scripts', 100 );


		$this->loader->add_action( 'add_meta_boxes', $plugin_careers, 'add_cvm_careers_meta_box' );
		$this->loader->add_action( 'save_post', $plugin_careers, 'save_cvm_careers_meta_box' );


		// $this->loader->add_action( 'admin_menu', $plugin_procedure_pricing, 'admin_menu' );
		// $this->loader->add_action( 'admin_init', $plugin_procedure_pricing, 'register_settings' );


		$this->loader->add_action( 'add_meta_boxes', $plugin_procedure_pricing, 'add_cvm_procedure_pricing_meta_box' );
		$this->loader->add_action( 'save_post', $plugin_procedure_pricing, 'save_cvm_procedure_pricing_meta_box' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Canyon_View_Medical_Public( $this->get_plugin_name(), $this->get_version() );
		$plugin_procedure_pricing = new Canyon_View_Medical_Procedure_Pricing( $this->get_plugin_name(), $this->get_version() );
		$plugin_rss = new Canyon_View_Medical_RSS( $this->get_plugin_name() . '_RSS', $this->get_version() );

		$this->loader->add_filter( 'the_content', $plugin_public, 'display_cvm_careers');
		$this->loader->add_filter( 'the_content', $plugin_procedure_pricing, 'display_cvm_procedure_pricing');
		$this->loader->add_filter('request', $plugin_public, 'alter_query');

		// $this->loader->add_filter('wpseo_opengraph_url', $plugin_public, 'change_post_opengraph_url' );


		$this->loader->add_action( 'init', $plugin_public, 'register_shortcodes' );
		$this->loader->add_action( 'init', $plugin_procedure_pricing, 'register_shortcodes' );
		$this->loader->add_action( 'init', $plugin_rss, 'register_shortcodes' );
		$this->loader->add_action( 'init', $plugin_rss, 'register_rss_feeds' );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_rss, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_rss, 'enqueue_scripts' );

		// $this->loader->add_action( 'pre_get_posts', $plugin_public, 'add_cat_to_query');
		$this->loader->add_action( 'rest_api_init', $plugin_rss, 'register_api_routes');
		$this->loader->add_action( 'rest_api_init', $plugin_public, 'add_thumbnail_to_JSON' );

		// Search queries
		$this->loader->add_action('pre_get_posts', $plugin_public, 'alter_search_query');

		// Ajax search form handlers
		$this->loader->add_action('wp_ajax_search_categories', $plugin_public, 'ajax_search_categories_handler');
		$this->loader->add_action('wp_ajax_nopriv_search_categories', $plugin_public, 'ajax_search_categories_handler');
		$this->loader->add_action('wp_ajax_search_authors', $plugin_public, 'ajax_search_authors_handler');
		$this->loader->add_action('wp_ajax_nopriv_search_authors', $plugin_public, 'ajax_search_authors_handler');
		$this->loader->add_filter( 'allowed_http_origins', $plugin_public, 'add_allowed_ajax_origins' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Canyon_View_Medical_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
