<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://mangledmonkeymedia.com
 * @since      1.0.0
 *
 * @package    Canyon_View_Medical
 * @subpackage Canyon_View_Medical/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Canyon_View_Medical
 * @subpackage Canyon_View_Medical/includes
 * @author     Mangled Monkey Media <brandon@mangledmonkeymedia.com>
 */
class Canyon_View_Medical_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'canyon-view-medical',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
