<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://mangledmonkeymedia.com
 * @since      1.0.0
 *
 * @package    Canyon_View_Medical
 * @subpackage Canyon_View_Medical/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Canyon_View_Medical
 * @subpackage Canyon_View_Medical/public
 * @author     Mangled Monkey Media <brandon@mangledmonkeymedia.com>
 */
class Canyon_View_Medical_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Canyon_View_Medical_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Canyon_View_Medical_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/canyon-view-medical-public.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name . '-search', plugin_dir_url( __FILE__ ) . 'css/canyon-view-medical-blog-search.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Canyon_View_Medical_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Canyon_View_Medical_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/canyon-view-medical-public.js', array( 'jquery' ), $this->version, false );
		wp_register_script( $this->plugin_name . '-search', plugin_dir_url( __FILE__ ) . 'js/canyon-view-medical-blog-search.js', array( 'jquery' ), $this->version, false );

		if ( $this->is_local_call() ) {
			$ajax_url = admin_url( 'admin-ajax.php' );
		} else {
			$ajax_url = 'https://canyonviewmedical.com/wp-admin/admin-ajax.php';
		}
		wp_localize_script( $this->plugin_name . '-search', 'search_ajax_object',	
			array( 'ajaxurl' => $ajax_url ) );

	}

	/**
	 * Register the shortcodes for the public-facing side of the site.
	 *
	 * @since    1.1.0
	 */
	public function register_shortcodes() {

		add_shortcode( 'cvm_careers_list', array( $this, 'cvm_careers_list_shortcode' ) );
		add_shortcode( 'cvm_search', array( $this, 'cvm_search_shortcode' ) );
		add_shortcode( 'cvm_more_stories_link', array( $this, 'cvm_more_stories_link_shortcode' ) );
		add_shortcode( 'cvm_service_posts', array( $this, 'cvm_service_posts_shortcode' ) );
	 }


	 /**
	 * Returns true if calls should be to local environment
	 *
	 * @since    1.6.0
	 */
	function is_local_call() {
		$dev = false;
		
		$site_url = get_site_url();

		if ( strpos( $site_url, 'localhost') !== false || strpos( $site_url, 'canyonviewmedical.com' ) !== false ) {
			$dev = true;
		}	

		return $dev;
	}
	 

	/**
	 * shortcode [cvmp_careers_list]
	 *
	 * @since    1.0.0
	 */
	public function cvm_careers_list_shortcode() {
		$careersPage = get_page_by_title("Careers");

		$childArgs = array(
		  'sort_order' => 'asc',
		  'sort_column' => 'post_title',
		  'hierarchical' => 1,
		  'exclude' => '',
		  'include' => '',
		  'meta_key' => '',
		  'meta_value' => '',
		  'authors' => '',
		  'child_of' => $careersPage->ID,
		  'parent' => -1,
		  'exclude_tree' => '',
		  'number' => '',
		  'offset' => 0,
		  'post_type' => 'page',
		  'post_status' => 'publish'
		);
		$childPages = get_pages($childArgs);

		$careersPageContent = '
		  <section id="jobListings">';

		foreach( $childPages as $child ) {
		  $jobTitle = $child->post_title;
		  $jobUrl = get_permalink( $child->ID );
		  $jobType = get_post_meta( $child->ID, '_cvm_careers_meta_job_type', 1);
		  $jobLocation = get_post_meta( $child->ID, '_cvm_careers_meta_job_location', 1);

		  $careersPageContent .= '
		    <a class="cvBtn" href="' . $jobUrl . '">
		      <span class="jlTitle">' . $jobTitle . '</span>
		      ' . $jobLocation . '  | ' . $jobType . '
		    </a>';
		}

		$careersPageContent .= '
		  </section>';

		return $careersPageContent;
	}

	/**
	 * Prints the provider met info to live page.
	 *
	 * @param WP_Post $post The object for the current post/page.
	 */
	public function display_cvm_careers ( $content )
	{
		global $post;
		$parentName = get_the_title($post->post_parent);

		// Add to page content if parent page is "Careers"
		if ( $parentName == "Careers" && $post->post_title != $parentName ) {
			$jobOrganization = get_post_meta( $post->ID, '_cvm_careers_meta_job_organization', 1);
			$jobLocation = get_post_meta( $post->ID, '_cvm_careers_meta_job_location', 1);
			$jobType = get_post_meta( $post->ID, '_cvm_careers_meta_job_type', 1);
			$jobIntroParagraph = get_post_meta( $post->ID, '_cvm_careers_meta_job_intro_paragraph', 1);
			$jobPrimaryResponsibilities = get_post_meta( $post->ID, '_cvm_careers_meta_job_primary_responsibilities', 1);
			$jobPositionQualifications = get_post_meta( $post->ID, '_cvm_careers_meta_job_position_qualifications', 1);
			$jobPreferredQualifications = get_post_meta( $post->ID, '_cvm_careers_meta_job_preferred_qualifications', 1);
			$jobPreferredExperience = get_post_meta( $post->ID, '_cvm_careers_meta_job_preferred_experience', 1);
			$jobRequiredExperience = get_post_meta( $post->ID, '_cvm_careers_meta_job_required_experience', 1);
			$jobPreferredEducation = get_post_meta( $post->ID, '_cvm_careers_meta_job_preferred_education', 1);
			$jobRequiredEducation = get_post_meta( $post->ID, '_cvm_careers_meta_job_required_education', 1);

			$jobDetails = array(
				array($jobPrimaryResponsibilities, "Primary Responsibilities"),
				array($jobPositionQualifications, "Position Qualifications"),
				array($jobPreferredQualifications, "Preferred Qualifications"),
				array($jobPreferredExperience, "Preferred Experience"),
				array($jobRequiredExperience, "Required Experience"),
				array($jobPreferredEducation, "Preferred Education"),
				array($jobRequiredEducation, "Required Education"),
			);


			$careerContent = '
				<div id="jobPage">
			  	<section id="jobDetails">
				    <h2>' . $jobOrganization . '</h2>
				    <h3>' . $jobLocation . '</h3>
				    <p class="intro">
				      ' . $jobIntroParagraph . '
				    </p>
						<h4>Job Type: ' . $jobType . '</h4>';

			foreach( $jobDetails as $detail ) {
				if ($detail[0] != '') {
					$careerContent .= '
						<h4>' . $detail[1] . ':</h4>
						<div>' . $detail[0] . '</div>';
				}

			}
			$careerContent .= '
					</section>
					[cvm_careers_list]
				</div>';

			$content = $careerContent . $content;
		}

		return $content;
	}

	/**
	 * Change post opengraph url for facebook sharing
	 *
	 * @since    1.4.0
	 */
	public function change_post_opengraph_url( $url ) 
	{
		if ( is_single() ) {
			global $post;
			$primary_id = get_post_meta($post->ID, '_yoast_wpseo_primary_category',true);
			$primary_cat = get_category($primary_id);
			$departments = array(
				['slug' => 'canyon-view-family-medicine', 'domain' => 'canyonviewfamilymedicine.com'],
				['slug' => 'canyon-view-orthopedics', 'domain' => 'canyonvieworthopedics.com'],
				['slug' => 'canyon-view-sports-medicine', 'domain' => 'canyonviewsportsmedicine.com'],
				['slug' => 'canyon-view-pediatrics', 'domain' => 'canyonviewpediatrics.com'],
				['slug' => 'canyon-view-womens-care', 'domain' => 'canyonviewwomenscare.com'],
			);

			foreach ( $departments as $department ) {
				if ( $department['slug'] === $primary_cat->slug ) {
					$url = str_replace('http://', 'https://', $url);
					$url = str_replace('canyonviewmedical.com', $department['domain'], $url);
				}
			}
		}

		return $url;
	}

	/**
	 * Filter primary category for Department posts
	 *
	 * @since    1.4.0
	 */
	function alter_query( $query_vars ) {

		$category = get_category_by_slug($query_vars['category_name'] );

		if ( substr( $category->slug, 0, 12 ) === 'canyon-view-' ) {
			
				$meta_query = array(
					'relation' => 'AND',
					array(
						'key'     => '_yoast_wpseo_primary_category',  
					'value'   => $category->term_id,
						'compare' => '=',
					),
					array(
						'key'     => '_yoast_wpseo_primary_category',  
						'compare' => 'EXISTS',
					),
					array(
						'key'     => '_yoast_wpseo_primary_category',  
						'value'   => '',
						'compare' => '!=',
					)
				);

				// Isolate Department category to meta_query vars
				$query_vars['category_name'] = '';	
				$query_vars['cat'] = '';
				$query_vars['tax_query'] = '';
				$query_vars['taxonomy'] = '';
				$query_vars['meta_query'] = $meta_query;

			}

		return $query_vars;

	}
		

	/**
	 * Filter for advanced search options
	 *
	 * @since    1.6.0
	 */
	function alter_search_query( $query ) {

		if ( !is_admin() && $query->is_main_query() && $query->is_search() && !$query->get( 'page_id' ) == get_option( 'page_on_front' ) ) {
			// Filter query by department
			$dep_slug = ( isset($_GET['department']) ? esc_attr($_GET['department']) : '' );


			if ( $dep_slug !== '' ) {
				$department = get_category_by_slug($dep_slug);
				$meta_query = array(
					'relation' => 'AND',
					array(
						'key'     => '_yoast_wpseo_primary_category',  
					'value'   => $department->term_id,
						'compare' => '=',
					),
					array(
						'key'     => '_yoast_wpseo_primary_category',  
						'compare' => 'EXISTS',
					),
					array(
						'key'     => '_yoast_wpseo_primary_category',  
						'value'   => '',
						'compare' => '!=',
					)
				);

				$query->set('meta_query', $meta_query);

			}

			$post_author = ( isset($_GET['post_author']) ? $_GET['post_author'] : '' ); 
			$query->set('author_name', $post_author);

			// Filter query by post type
			$post_type = ( isset($_GET['post_type']) ? $_GET['post_type'] : array('post') );
			$query->set('post_type', $post_type);
			
			
		}


	}


	/**
	 * Add the post featured image url to REST API object
	 * @since    1.5.00
	 */
	public function add_thumbnail_to_JSON() {
		//Add featured image
		register_rest_field( 
				'post', // Where to add the field (Here, blog posts. Could be an array)
				'featured_image_src', // Name of new field (You can call this anything)
				array(
						'get_callback'    => function ( $object, $field_name, $request ) {
							 $feat_img_array = wp_get_attachment_image_src(
								 $object['featured_media'], // Image attachment ID
								 'thumbnail',  // Size.  Ex. "thumbnail", "large", "full", etc..
								 true // Whether the image should be treated as an icon.
							 );
							 return $feat_img_array[0];
						 },
						'update_callback' => null,
						'schema'          => null,
						 )
				);
		}

	/**
	 * 
	 * Return an array of authors with posts
	 *
	 * @since    1.6.01
	 */

		function get_all_authors( $department = '' ) {
			global $wpdb;
	
			foreach ( $wpdb->get_results("SELECT DISTINCT post_author, COUNT(ID) AS count FROM $wpdb->posts WHERE post_type = 'post' AND " . get_private_posts_cap_sql( 'post' ) . " GROUP BY post_author") as $row ) :
					$author = get_userdata( $row->post_author );
					$authors[$row->post_author]['name'] = $author->display_name;
					$authors[$row->post_author]['post_count'] = $row->count;
					$authors[$row->post_author]['posts_url'] = get_author_posts_url( $author->ID, $author->user_nicename );
					$authors[$row->post_author]['nicename'] = $author->user_nicename;

			endforeach;
	
			return $authors;
	}

		
	/**
	 * shortcode [cvm_search]
	 * 
	 * The custom search tool for blog pages
	 *
	 * @since    1.6.00
	 */
	public function cvm_search_shortcode() {
		$term = esc_attr( $_GET['s'] );
		$dep = ( isset($_GET['department']) ? $_GET['department'] : '' );
		$post_author = ( isset($_GET['post_author']) ? $_GET['post_author'] : '' ); 
		// $post_type = ( isset($_GET['post_type']) ? $_GET['post_type'] : array('post') );
		$post_format = ( isset($_GET['post_format']) ? $_GET['post_format'] : array() );
		
		// Load the search form JS
		wp_enqueue_script( $this->plugin_name . '-search' );

		if ( $this->is_local_call() ) {
			$action = home_url('/'); 
			$target = '_self';
		} else {
			$action = 'https://canyonviewmedical.com/';
			$target = '_blank';
		}
		
		$departments = array(
			['slug' => 'canyon-view-family-medicine', 'name' => 'Family Medicine'],
			['slug' => 'canyon-view-pediatrics', 'name' => 'Pediatrics'],
			['slug' => 'canyon-view-womens-care', 'name' => 'Women\'s Care'],
			['slug' => 'canyon-view-orthopedics', 'name' => 'Orthopedics'],
			['slug' => 'canyon-view-sports-medicine', 'name' => 'Sports Medicine'],
			['slug' => 'canyon-view-medical', 'name' => 'Canyon View Medical'],
		);

		require( plugin_dir_path( __FILE__ ) . 'partials/canyon-view-medical-blog-search.php' );	

	}


	/**
	 *  
	 * Add remote domains to permitted ajax request origins
	 *
	 * @since    1.6.04
	 */
	function add_allowed_ajax_origins( $origins ) {
    $origins[] = 'https://canyonviewfamilymedicine.com';
    $origins[] = 'https://canyonvieworthopedics.com';
    $origins[] = 'https://canyonviewsportsmedicine.com';
    $origins[] = 'https://canyonviewpediatrics.com';
    $origins[] = 'https://canyonviewwomenscare.com';
		return $origins;
	}


	/**
	 *  
	 * Ajax response to filter categories list by primary category
	 *
	 * @since    1.6.00
	 */
	function ajax_search_categories_handler() {
		// global $wp_query;
		$department = esc_attr( $_POST['department'] );
		$selected_category = esc_attr($_POST['selected_category'][1]);
		
		$data = $this->get_categories_by_department( $department, $selected_category );

		// Return filtered categories
		wp_send_json($data);

		die();
	}


		/**
	 *  
	 * Ajax response to filter categories list by primary category
	 *
	 * @since    1.6.05
	 */
	function ajax_search_authors_handler() {
		$categories_data = $_POST['categories_data'];
		$department = esc_attr( $_POST['department'] );
		$selected_category = esc_attr( $_POST['selected_category'] );
		$selected_author = esc_attr($_POST['selected_author'][1]);
		$categories = array();
		$category_posts = array();
		$authors = array();

		// Use categories data array if available
		if ( !empty( $categories_data['categories'] && $selected_category === '') ) {
			// Use data array
			$categories = $categories_data['categories'];

		} elseif ( $selected_category !== '' ) {
			// Get the selected category
			$categories[0] = get_category_by_slug( $selected_category );

		} else {
			// Get all category filtered by department
			$department_data = $this->get_categories_by_department( $department, $selected_category );
			$categories = $department_data['categories'];
		}

		// Get list of posts filtered by department categories
		foreach ( $categories as $category ) {
			$posts = $this->get_category_posts( $department, $category );

			foreach ( $posts as $post ) {
				array_push($category_posts, $post);
			}

		}

		// Get the author of each post
		foreach ( $category_posts as $category_post ) {
			$post_author = $category_post->post_author;
			$post_author_name = get_the_author_meta('display_name', $post_author);
			$post_author_lastname = get_the_author_meta('last_name', $post_author);
			$post_author_nicename = get_the_author_meta('user_nicename', $post_author);

			// Remove 'Canyon View ...' authors
			if ( substr( $post_author_name, 0, 12 ) !== 'Canyon View ') {
				$author = array(
					'name' => $post_author_name,
					'lastname' => $post_author_lastname,
					'nicename' => $post_author_nicename
				);

				array_push( $authors, $author );

			}
		}

		// Remove duplicate authors and sort ASC
		$authors = array_unique($authors, SORT_REGULAR);
		usort($authors, function($a, $b) {return strcmp($a['lastname'], $b['lastname']);});

		// Format data to be returned
		$data = [
			'selected_author' => $selected_author,
			'authors' => $authors,
		];

		// Return filtered authors
		wp_send_json($data);

		die();

	}


	/**
	 *  
	 * Return an array of categories for the given department
	 *
	 * @since    1.6.05
	 */
	function get_categories_by_department( $department, $selected_category ) {
		$department = get_category_by_slug( $department );
		$selected_category = get_category_by_slug( $selected_category );
		$meta_query = '';

		// Pull list of categories
		if ( !empty( $department ) ) {
			$meta_query = array(
				'relation' => 'AND',
				array(
					'key'     => '_yoast_wpseo_primary_category',  
				'value'   => $department->term_id,
					'compare' => '=',
				),
				array(
					'key'     => '_yoast_wpseo_primary_category',  
					'compare' => 'EXISTS',
				),
				array(
					'key'     => '_yoast_wpseo_primary_category',  
					'value'   => '',
					'compare' => '!=',
				)
			);
		} 

		$query_args = array(
			//basic stuff
			'post_status' => 'publish',
			'posts_per_page' => '-1',
			'meta_query' => $meta_query
			);

		$the_query = new WP_Query( $query_args );

		//get categories from posts and amalgamate them
		$categories = array();

		if($the_query->have_posts()){
			$the_posts = $the_query->posts;

			foreach( $the_posts as $the_post ) {
				// error_log('post->ID: ' . $post->ID );
				$new_cats = wp_get_object_terms( $the_post->ID, 'category' );

				foreach ( $new_cats as $cat ) {
					// Filter out department categories
					if ( substr( $cat->slug, 0, 12 ) !== 'canyon-view-' ) {
						// Add category to array
						array_push($categories, $cat);
					}

				}
				
			}

			// Remove duplicate categories and sort ASC
			$categories = array_unique($categories, SORT_REGULAR);
			usort($categories, function($a, $b) {return strcmp($a->slug, $b->slug);});

		}

		$data = [
			'department' => $department,
			'selected_category' => $selected_category,
			'categories' => $categories,
		];

		return $data;

	}


	/**
	 *  
	 * Return an array of posts for the given category
	 *
	 * @since    1.6.05
	 */
	function get_category_posts( $department, $category ) {
		$department = get_category_by_slug( $department );
		$posts = array();

		$query_args = array(
			'numberposts'      => -1,
			'category'         => $category->term_id,
			'post_type'        => 'post',
			'suppress_filters' => true,
		);

		// Get all posts in department categories
		$the_posts = get_posts( $query_args );

		// Filter posts by department
		if( ! empty( $department ) ){

			foreach( $the_posts as $the_post ) {
				$primary_category = (int)get_post_meta ($the_post->ID, '_yoast_wpseo_primary_category', true);

				if ( $primary_category === $department->term_id ) {
					array_push($posts, $the_post);
				}
			}

		} else {
			$posts = $the_posts;

		}

		return $posts;
	}


	/**
	 * shortcode [cvm_more_stories_link]
	 * 
	 * A paginated more stories link for blog sidebar
	 *
	 * @since    1.6.28
	 */
	public function cvm_more_stories_link_shortcode() {

		if( $url = get_next_posts_page_link() ):
			return '<a href="' . $url . '" class="sidebar-read-more">More Stories</a>';
		endif;		

	}


	/**
	 * Returns a data object via WP API
	 *
	 * @since    1.6.29
	 */
	function get_remote_data( $type, $args )
	{
		$args_string = '';

		if ( !empty( $args ) ) {
			$args_string = '?';
			$arg_count = 0;

			foreach ( $args as $key => $value ) {
				$args_string .= ( $arg_count > 0 ? '&' : '' ) . $key . '=' . $value;
				$arg_count++;
			}
		}

			// error_log($args_string);
		$remote = 'https://canyonviewmedical.com/wp-json/wp/v2/' . $type . $args_string;

		if ( $remote !== '' ) {
			$request = wp_remote_get( $remote );

			if( is_wp_error( $request ) ) {
				return false; // Bail early
			}
			
			$body = wp_remote_retrieve_body( $request );

			return json_decode( $body, true );
		}
	}

	/**
	 * Returns a full name string for a provider
	 *
	 * @since    1.6.29
	 */
	 function get_service_posts( $post_service, $limit = -1 )
	 
	{
		$posts = [];

		$post_args = array(
			'filter[category_name]'	=> $post_service,
			'per_page' 							=> $limit,
			'status'								=> 'publish',
			'orderby'						  	=> 'date',
			'order'  						  	=> 'desc',
		);

		$posts = $this->get_remote_data( 'posts', $post_args );
		
		// var_dump($posts);

		return $posts;
	}

	/**
	 * shortcode [cvm_service_posts]
	 * 
	 * The most recent posts related to a blog article
	 *
	 * @since    1.6.29
	 */
	public function cvm_service_posts_shortcode($atts) {
		global $post;
    $atts = shortcode_atts( array(
			'service' => ''
		), $atts, 'cvm_service_posts' );

		// Set service category
		if ( empty($atts['service']) && get_the_title($post->post_parent) == 'Services') {
			$service = get_the_title($post->ID);
		} else {
			$service = $atts['service'];
		}

		// Service posts as stories
		if ( $service ) {
			$postsLimit = 5;
			$servicePosts = $this->get_service_posts( $service, $postsLimit );

			if ( !empty($servicePosts) ) {
				$postsDomain = 'https://canyonviewmedical.com';
				$serviceSlug = str_replace(' ','-',$service);
				$servicePostsUrl = $postsDomain . '/?s=&department=&post_author=&post_type=post&post_format=&category_name=' . $serviceSlug;

				$stories = '
					<div id="service-posts">
						<section class="stories">
							<h5>Stories related to ' . ucwords(esc_attr( $service )) . ':</h5>';

				foreach ( $servicePosts as $servicePost ) {
					$postTitle = $servicePost['title']['rendered'];
					$postLink = $postsDomain . $servicePost['link'];
					$postThumbnailUrl = $postsDomain . $servicePost['featured_image_src'];

					$stories .= '
						<a class="serviceStory" href="' . $postLink . '" target="_blank">
								<span class="storyThumb" style="background-image: url(' . $postThumbnailUrl . ');">&nbsp;</span>
								<span class="storyTitle">' . $postTitle . '</span>
						</a>';
				}

				if ( count($servicePosts) === $postsLimit ) {
					$stories .= '
						<div class="see-more-posts">
							<a href="' . $servicePostsUrl . '" class="" target="_blank">See more stories. . .</a>
						</div>';
				}

				$stories .= '
					</section>
				</div>'; // #service-posts

				return $stories;
			}
		}

	}


}
