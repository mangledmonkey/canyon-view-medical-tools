(function( $ ) {
	'use strict';

	$(function() {
		// Departments onchange
		update_categories()
		$('#departments input[type=radio]').change(update_categories)
		$('#categories select').change(update_authors)
		$('#reset .reset-search-form').click(reset_form_selections)
	});
			
	function update_categories(){
		var department = $('input[name=department]:checked', '#departments').val()
		var selected_category = new RegExp('[\?&]category_name=([^&#]*)').exec(window.location.href);
		
		var data = {
			'action': 'search_categories',
			'department': department,
			'selected_category': selected_category
		};
	
		$.ajax({
			url : search_ajax_object.ajaxurl,
			data : data,
			dataType:'json',
			type : 'POST',
			cache: false,
			beforeSend : function ( xhr ) {
				$('#categories select').html('<option>Loading...</option>')
				$('#categories select').prop( 'disabled', true )
			},
			success : function( categories_data ) {
				if ( categories_data ) {
					var selected_category = categories_data['selected_category']['slug']
					var categories = categories_data['categories']
					$('#categories select').html('<option value="" id="cat-all">All Categories</option>')
					$.each( categories, function(index, value) {
						if ( value['slug'] === selected_category ) {
							$('#categories select').append(`<option value="${value['slug']}" selected="selected">
							${value['name']}
						</option>`); 
						} else {
							$('#categories select').append(`<option value="${value['slug']}">
								${value['name']}
							</option>`); 
						}
					})
					$('#categories select').prop( 'disabled', false )
				} else {
					$('#categories select').html('<option>No Categories</option>')
				}

				update_authors( categories_data )
			}
		});
	};


	function update_authors( categories_data ){
		var department = $('input[name=department]:checked', '#departments').val()
		var selected_category = $('#categories select').val()
		var selected_author = new RegExp('[\?&]post_author=([^&#]*)').exec(window.location.href)

		if ( categories_data['type'] === 'change' ) {
			var categories_data = []
		}
		
		var data = {
			'action': 'search_authors',
			'department': department,
			'selected_category': selected_category,
			'categories_data': categories_data,
			'selected_author': selected_author
		};
	
		$.ajax({
			url : search_ajax_object.ajaxurl,
			data : data,
			dataType:'json',
			type : 'POST',
			cache: false,
			beforeSend : function ( xhr ) {
				$('#authors select').html('<option>Loading...</option>')
				$('#authors select').prop( 'disabled', true )
			},
			success : function( authors_data ) {
				if ( authors_data ) {
					var selected_author = authors_data['selected_author']
					var authors = authors_data['authors']
					$('#authors select').html('<option value="" id="auth-all">All Authors</option>')
					$.each( authors, function(index, value) {
						if ( value['nicename'] === selected_author ) {
							$('#authors select').append(`<option value="${value['nicename']}" selected="selected">
							${value['name']}
						</option>`); 
						} else {
							$('#authors select').append(`<option value="${value['nicename']}">
								${value['name']}
							</option>`); 
						}
					})
					$('#authors select').prop( 'disabled', false )
				} else {
					$('#authors select').html('<option>No Categories</option>')
				}
			}
		});
	};

	function hide_show_search(){
		$(this).toggle(
			function(){$("#feed-search #the-form").css({"display": "none"});},
			function(){$("#feed-search #the-form").css({"display": "block"});},
		)
	};

	function reset_form_selections(){
		$('#search-field .search-field').val('')
		$('#departments #dep-all').prop("checked", true ) 
		$('#categories #cat-all').prop("selected", true )
		$('#authors #auth-all').prop("selected", true )
		$('#media-types #med-all').prop("checked", true )
		update_categories()
	};

})( jQuery );
