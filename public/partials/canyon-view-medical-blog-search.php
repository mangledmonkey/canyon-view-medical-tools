<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://mangledmonkeymedia.com
 * @since      1.6.0
 *
 * @package    Canyon_View_Medical
 * @subpackage Canyon_View_Medical/public/partials
 */
?>
<div id="feed-search">
  <div id="the-form">
    <form id="searchform" method="get" action="<?php echo $action ?>" target="<?php echo $target ?>">

      <div id="search-field" class="search-item-box">
        <input type="text" class="search-field" name="s" placeholder="Search" value="<?php echo $term ?>">
      </div>

      <h6>Filters</h6>

      <div id="departments" class="search-item-box">
        <label for="department">Departments</label><br>
        <input type="radio" name="department" id="dep-all" value=""<?php echo ( $dep === '' ? ' checked="checked"' : '' ) ?>> All<br>
      <? foreach ($departments as $department ) { ?>
        <input type="radio" name="department" value="<?php echo $department['slug'] ?>"<?php echo ( $dep === $department['slug'] ? ' checked="checked"' : '' ) ?>> <?php echo $department['name'] ?><br>
      <?php } ?>
      </div>

      <div id="categories" class="search-item-box">
        <label for="cat">Categories</label><br>
        <select name="category_name" id="cat" class="postform blog-dropdown">
          <option value="" id="cat-all">All Categories</option>
        </select>
      </div>

      <div id="authors" class="search-item-box">
        <label for="auth">Authors</label><br>
        <select name="post_author" id="auth" class="postform blog-dropdown">
          <option value="" id="auth-all">All Authors</option>
        </select>
      </div>

      <div id="media-types" class="search-item-box">
        <label for="types">Media Types</label><br />

        <input type="hidden" name="post_type" value="post" >
        <input type="radio" name="post_format" value=""<?php echo ( ( empty($post_format) || $post_format === '' ) ? ' checked="checked"' : '' ) ?> id="med-all"> All<br>
        <input type="radio" name="post_format" value="post-format-video"<?php echo ( ($post_format === 'post-format-video') ? ' checked="checked"' : '' ) ?>> Videos<br>
        <input type="radio" name="post_format" value="post-format-audio"<?php echo ( ($post_format === 'post-format-audio' ) ? ' checked="checked"' : '' ) ?>> Podcasts<br>

      </div>

      <div id="reset" class="search-item-box">
        <a href="javascript:void(0)" class="reset-search-form">Reset Filters</a>
      </div>

      <input type="submit" value="Search">
    </form>
   </div> <!-- /the-form -->
</div> <!-- /feed-search -->