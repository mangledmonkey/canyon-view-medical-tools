<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://mangledmonkeymedia.com
 * @since             1.0.0
 * @package           Canyon_View_Medical
 *
 * @wordpress-plugin
 * Plugin Name:       Canyon View Medical Tools
 * Plugin URI:        none
 * Description:       Provides custom tools for managing the pages on the Canyon View Medical website.
 * Version:           1.7.9
 * Author:            Mangled Monkey Media
 * Author URI:        http://mangledmonkeymedia.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       canyon-view-medical
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-canyon-view-medical-activator.php
 */
function activate_canyon_view_medical() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-canyon-view-medical-activator.php';
	Canyon_View_Medical_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-canyon-view-medical-deactivator.php
 */
function deactivate_canyon_view_medical() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-canyon-view-medical-deactivator.php';
	Canyon_View_Medical_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_canyon_view_medical' );
register_deactivation_hook( __FILE__, 'deactivate_canyon_view_medical' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-canyon-view-medical.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_canyon_view_medical() {

	$plugin = new Canyon_View_Medical();
	$plugin->run();

}
run_canyon_view_medical();
