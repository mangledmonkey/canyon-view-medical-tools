(function($) {
  'use strict';

  /**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

  $('body').on('click', '.cvm-new-procedure', function () {
    var sectionId = $(this).parent().attr('id').replace(/.*-/g, '');
    var procedureId = $(this).parent().find('table.rootlist').children('tbody').children().length;
    var appendProcedure = '<tr id="cvm-procedure-' + procedureId + '"><td><input type="text" name="cvm_procedure_pricing_meta_sections[' + sectionId + '][procedures][' + procedureId + '][cpt-code]" value="" size="5" /></td><td><input type="text" name="cvm_procedure_pricing_meta_sections[' + sectionId + '][procedures][' + procedureId + '][name]" value="" size="40" /></td><td><input type="text" name="cvm_procedure_pricing_meta_sections[' + sectionId + '][procedures][' + procedureId + '][price]" value="" size="5" /></td><td><button class="button cvm cvm-remove-procedure" type="button">remove</button></td></tr>';
    $(this).parent().find('table.rootlist').append(appendProcedure);
  });

  $('body').on('click', '.cvm-remove-procedure', function () {
    var count = 0;
    var items = $(this).closest('table.rootlist');
    // $(this).closest("ul[class*='cvm-procedure-']").parent('li').remove();
    $(this).closest('tr').remove();
    items.find("tr[id*='cvm-procedure-']").each(function () {
      $(this).html(function (index, text) {
        return text.replace(/\[procedures\]\[[0-9]{1,2}\]/g, '[procedures][' + count + ']');
      });
      count++;
    });
  });

  $('button.cvm-new-section').on('click', function () {
    var lastSection = $(this).parent().find('div[id*="cvm-section-"]').last().attr('id').replace(/.*-/g, '');
    var sectionId = parseInt(lastSection) + 1;
    var headingId = sectionId + 1;
    var appendSection = '<div id="cvm-section-' + sectionId + '" class="cvm-section"><div class="cvm-section-header"><input type="text" id="hidden-section-heading-' + sectionId + '" name="cvm_procedure_pricing_meta_sections[' + sectionId + '][heading]" value="Section ' + headingId + '" class="cvm-section-heading" /></div><table class="rootlist"><thead><th width="10%">CPT Code</th><th width="70%">Procedure</th><th width="10%">Price ($)</th><th width="10%"></th></thead><tbody><tr id="cvm-procedure-0"><td><input type="text" name="cvm_procedure_pricing_meta_sections[' + sectionId + '][procedures][0][cpt-code]" value="" size="5" /></td><td><input type="text" name="cvm_procedure_pricing_meta_sections[' + sectionId + '][procedures][0][name]" value="" size="40" /></td><td><input type="text" name="cvm_procedure_pricing_meta_sections[' + sectionId + '][procedures][0][price]" value="" size="5" /></td><td><button class="button cvm cvm-remove-procedure" type="button">remove</button></td></tr></tbody></table><button class="button cvm cvm-new-procedure" type="button">+ New Procedure</button><button class="button cvm cvm-remove-section" type="button">Remove Section</button></div>';
    $('div#cvm_procedure_sections').append(appendSection);
  });

  $('body').on('click', '.cvm-remove-section', function () {
    if (confirm('Are you sure you want to remove this section?\nChanges will not be permanent until the page is saved.')) {
      var count = 0;
      var sections = $(this).closest('div#cvm_procedure_sections');
      $(this).closest("div[id*='cvm-section-']").remove();
      sections.find("div[id*='cvm-section-']").each(function () {

        $(this).attr('id', function (i, id) {
          if (/cvm-section-[0-9]{1,2}/.test(id)) {
            return id.replace(/cvm-section-[0-9]{1,2}/g, 'cvm-section-' + count);
          }
        });

        $(this).find("input[name*='meta_sections']").attr('name', function (i, name) {
          if (/sections\[[0-9]{1,2}\]/.test(name)) {
            return name.replace(/sections\[[0-9]{1,2}\]/g, 'sections[' + count + ']');
          }
        });

        count++;
      });
    };

  });

})(jQuery);
