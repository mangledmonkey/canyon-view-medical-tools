<?php

/**
 * The Careers-specific functionality of the plugin.
 *
 * @link       http://mangledmonkeymedia.com
 * @since      1.0.0
 *
 * @package    Canyon_View_Medical
 * @subpackage Canyon_View_Medical/Careers
 */

/**
 * The Careers-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the Careers-specific stylesheet and JavaScript.
 *
 * @package    Canyon_View_Medical
 * @subpackage Canyon_View_Medical/Careers
 * @author     Mangled Monkey Media <brandon@mangledmonkeymedia.com>
 */
class Canyon_View_Medical_Careers {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the Careers area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Canyon_View_Medical_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Canyon_View_Medical_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/canyon-view-medical-careers.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the Careers area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Canyon_View_Medical_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Canyon_View_Medical_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/canyon-view-medical-careers.js', array( 'jquery' ), $this->version, false );

	}

	/**
  * Add the meta box to the page(s)
  *
  * @since		1.0.0
  */
	public function add_cvm_careers_meta_box() {
		global $post;
		$parentName = get_the_title($post->post_parent);
		if ( $parentName == "Careers" && $post->post_title != $parentName ) {
		  add_meta_box(
		      'cvm_careers_meta_box',
		      __( 'Canyon View Medical Careers—Job Information', 'cvm_careers_textdomain' ),
		      array( $this, 'cvm_careers_meta_box_callback' ),
		      'page'
	  	);
		}
	}

	/**
	 * Prints the meta box content.
	 *
	 * @param WP_Post $post The object for the current post/page.
	 */
	public function cvm_careers_meta_box_callback( $post ) {

		// Add a nonce field so we can check for it later.
		wp_nonce_field( 'cvm_careers_meta_box', 'cvm_careers_meta_box_nonce' );

		/*
		 * Use get_post_meta() to retrieve an existing value
		 * from the database and use the value for the form.
		 */
		$jobOrganization = get_post_meta( $post->ID, '_cvm_careers_meta_job_organization', true );
		$jobLocation = get_post_meta( $post->ID, '_cvm_careers_meta_job_location', true );
		$jobType = get_post_meta( $post->ID, '_cvm_careers_meta_job_type', true );
		$jobIntroParagraph = get_post_meta( $post->ID, '_cvm_careers_meta_job_intro_paragraph', true );
		$jobPrimaryResponsibilities = get_post_meta( $post->ID, '_cvm_careers_meta_job_primary_responsibilities', true );
		$jobPositionQualifications = get_post_meta( $post->ID, '_cvm_careers_meta_job_position_qualifications', true );
		$jobPreferredQualifications = get_post_meta( $post->ID, '_cvm_careers_meta_job_preferred_qualifications', true );
		$jobPreferredExperience = get_post_meta( $post->ID, '_cvm_careers_meta_job_preferred_experience', true );
		$jobRequiredExperience = get_post_meta( $post->ID, '_cvm_careers_meta_job_required_experience', true );
		$jobPreferredEducation = get_post_meta( $post->ID, '_cvm_careers_meta_job_preferred_education', true );
		$jobRequiredEducation = get_post_meta( $post->ID, '_cvm_careers_meta_job_required_education', true );

		$editorSettings = array(
  		'media_buttons' => false,
  		'tinymce' => array(
      	'theme_advanced_buttons1' => 'formatselect,|,bold,italic,underline,|,' .
        'bullist,blockquote,|,justifyleft,justifycenter' .
        ',justifyright,justifyfull,|,link,unlink,|' .
        ',spellchecker,wp_fullscreen,wp_adv'
  	));

	  ?>
	  <div id="cvm-careers-meta-box">

	    <p>
		  	<label for="cvm-careers-meta-job-organization" class="cvm-careers-row-title">
				<?php _e( 'Organization', 'cvm_careers_textdomain' ); ?>
			  </label>
	  		<input type="text" id="cvm_careers_meta_job_organization" name="cvm_careers_meta_job_organization" value="<?php echo esc_attr( $jobOrganization ) ?>" size="25" />
			</p>
			<p>
		  	<label for="cvm-careers-meta-job-location" class="cvm-careers-row-title">
				<?php _e( 'Location', 'cvm_careers_textdomain' ); ?>
			  </label>
	  		<input type="text" id="cvm_careers_meta_job_location" name="cvm_careers_meta_job_location" value="<?php echo esc_attr( $jobLocation) ?>" size="25" />
			</p>
			<p>
		  	<label for="cvm-careers-meta-job-type" class="cvm-careers-row-title">
				<?php _e( 'Job Type', 'cvm_careers_textdomain' ); ?>
			  </label>
	  		<input type="text" id="cvm_careers_meta_job_type" name="cvm_careers_meta_job_type" value="<?php echo esc_attr( $jobType) ?>" size="25" />
			</p>
			<p>
				<label for="cvm-careers-meta-job-intro-paragraph" class="cvm-row-title">
					<?php _e( 'Intro Paragraph', 'cvfmp-textdomain');?>
				</label>
				<?php wp_editor( stripslashes( $jobIntroParagraph ), 'cvm_careers_meta_job_intro_paragraph', $editorSettings ); ?>
			</p>
			<p>
				<label for="cvm-careers-meta-job-primary-responsibilities" class="cvm-row-title">
					<?php _e( 'Primary Responsibilities', 'cvfmp-textdomain');?>
				</label>
				<?php wp_editor( stripslashes( $jobPrimaryResponsibilities ), 'cvm_careers_meta_job_primary_responsibilities', $editorSettings ); ?>
			</p>
			<p>
				<label for="cvm-careers-meta-job-position-qualifications" class="cvm-row-title">
					<?php _e( 'Position Qualifications', 'cvfmp-textdomain');?>
				</label>
				<?php wp_editor( stripslashes( $jobPositionQualifications ), 'cvm_careers_meta_job_position_qualifications', $editorSettings ); ?>
			</p>
			<p>
				<label for="cvm-careers-meta-job-preferred-qualifications" class="cvm-row-title">
					<?php _e( 'Preferred Qualifications', 'cvfmp-textdomain');?>
				</label>
				<?php wp_editor( stripslashes( $jobPreferredQualifications ), 'cvm_careers_meta_job_preferred_qualifications', $editorSettings ); ?>
			</p>
			<p>
				<label for="cvm-careers-meta-job-preferred-experience" class="cvm-row-title">
					<?php _e( 'Preferred Experience', 'cvfmp-textdomain');?>
				</label>
				<?php wp_editor( stripslashes( $jobPreferredExperience ), 'cvm_careers_meta_job_preferred_experience', $editorSettings ); ?>
			</p>
			<p>
				<label for="cvm-careers-meta-job-required-experience" class="cvm-row-title">
					<?php _e( 'Required Experience', 'cvfmp-textdomain');?>
				</label>
				<?php wp_editor( stripslashes( $jobRequiredExperience ), 'cvm_careers_meta_job_required_experience', $editorSettings ); ?>
			</p>
			<p>
				<label for="cvm-careers-meta-job-preferred-education" class="cvm-row-title">
					<?php _e( 'Preferred Education', 'cvfmp-textdomain');?>
				</label>
				<?php wp_editor( stripslashes( $jobPreferredEducation ), 'cvm_careers_meta_job_preferred_education', $editorSettings ); ?>
			</p>
			<p>
				<label for="cvm-careers-meta-job-required-education" class="cvm-row-title">
					<?php _e( 'Required Education', 'cvfmp-textdomain');?>
				</label>
				<?php wp_editor( stripslashes( $jobRequiredEducation ), 'cvm_careers_meta_job_required_education', $editorSettings ); ?>
			</p>

	  </div>
	  <?php
	}


	/**
	 * Save the meta when the post is saved.
	 *
	 * @param int $post_id The ID of the post being saved.
	 */
	public function save_cvm_careers_meta_box( $post_id ) {

		/*
		 * We need to verify this came from the our screen and with proper authorization,
		 * because save_post can be triggered at other times.
		 */

		// Check if our nonce is set.
		if ( ! isset( $_POST['cvm_careers_meta_box_nonce'] ) )
			return $post_id;

		$nonce = $_POST['cvm_careers_meta_box_nonce'];

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $nonce, 'cvm_careers_meta_box' ) )
			return $post_id;

		// If this is an autosave, our form has not been submitted,
	            //     so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return $post_id;

		// Check the user's permissions.
		if ( 'page' == $_POST['post_type'] ) {

			if ( ! current_user_can( 'edit_page', $post_id ) )
				return $post_id;

		} else {

			if ( ! current_user_can( 'edit_post', $post_id ) )
				return $post_id;
		}

		/* OK, its safe for us to save the data now. */

	  // Sanitize the user input.
	  $jobOrganization = sanitize_text_field( $_POST['cvm_careers_meta_job_organization'] );
	  $jobLocation = sanitize_text_field( $_POST['cvm_careers_meta_job_location'] );
	  $jobType = sanitize_text_field( $_POST['cvm_careers_meta_job_type'] );
		$jobIntroParagraph = $_POST['cvm_careers_meta_job_intro_paragraph'];
		$jobPrimaryResponsibilities= $_POST['cvm_careers_meta_job_primary_responsibilities'];
		$jobPositionQualifications = $_POST['cvm_careers_meta_job_position_qualifications'];
		$jobPreferredQualifications = $_POST['cvm_careers_meta_job_preferred_qualifications'];
		$jobPreferredExperience = $_POST['cvm_careers_meta_job_preferred_experience'];
		$jobRequiredExperience = $_POST['cvm_careers_meta_job_required_experience'];
		$jobPreferredEducation = $_POST['cvm_careers_meta_job_preferred_education'];
		$jobRequiredEducation = $_POST['cvm_careers_meta_job_required_education'];

	  // Update the meta field.
		update_post_meta( $post_id, '_cvm_careers_meta_job_organization', $jobOrganization);
		update_post_meta( $post_id, '_cvm_careers_meta_job_location', $jobLocation);
		update_post_meta( $post_id, '_cvm_careers_meta_job_type', $jobType);
		update_post_meta( $post_id, '_cvm_careers_meta_job_intro_paragraph', $jobIntroParagraph);
		update_post_meta( $post_id, '_cvm_careers_meta_job_primary_responsibilities', $jobPrimaryResponsibilities);
		update_post_meta( $post_id, '_cvm_careers_meta_job_position_qualifications', $jobPositionQualifications);
		update_post_meta( $post_id, '_cvm_careers_meta_job_preferred_qualifications', $jobPreferredQualifications);
		update_post_meta( $post_id, '_cvm_careers_meta_job_preferred_experience', $jobPreferredExperience);
		update_post_meta( $post_id, '_cvm_careers_meta_job_required_experience', $jobRequiredExperience);
		update_post_meta( $post_id, '_cvm_careers_meta_job_preferred_education', $jobPreferredEducation);
		update_post_meta( $post_id, '_cvm_careers_meta_job_required_education', $jobRequiredEducation);

	}

}
